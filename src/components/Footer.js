import React from 'react';
import { Link } from 'react-router-dom';
import {BsFacebook, BsTelegram, BsWhatsapp, BsTwitter} from 'react-icons/bs'

const Footer = () => {
  return <>
    <footer className='py-4'>
      <div className='container-xxl'>
        <div className='row'>
          <div className='col-4'>
            <h4 className='text-white mb-4'>Contact Us</h4>
            <div>
              <address className='text-white fs-6'>P7, Palao, Iligan City, <br/> Lanao del norte, <br/> Philippines, 9200</address>
              <a href='tel:+63 9123456789' className='mt-4 d-block mb-1 text-white'>+63 9123456789</a>
              <a href='mailto:ayei20@gmail.com' className='mt-4 d-block mb-0 text-white'>ayei20@gmail.com</a>
              <div className='social_icons d-flex align-items-center gap-30 mt-4'>
                <a className='text-white' href='#'>
                  <BsFacebook className='fs-4'/>
                </a>
                <a className='text-white' href='#'>
                  <BsTwitter className='fs-4'/>
                </a>
                <a className='text-white' href='#'>
                  <BsTelegram className='fs-4'/>
                </a>
                <a className='text-white' href='#'>
                  <BsWhatsapp className='fs-4'/>
                </a>
              </div>
            </div>
          </div>
          <div className='col-3'>
            <h4 className='text-white mb-4'>Information</h4>
            <div className='footer-links d-flex flex-column'>
            <Link className='text-white py-2 mb-1'>Privacy policy</Link>
            <Link className='text-white py-2 mb-1'>Refund Policy</Link>
            <Link className='text-white py-2 mb-1'>Shipping Policy</Link>
            <Link className='text-white py-2 mb-1'>Terms & Conditions</Link>
            </div>
          </div>
          <div className='col-3'>
            <h4 className='text-white mb-4'>Account</h4>
            <div className='footer-links d-flex flex-column'>
            <Link className='text-white py-2 mb-1'>About Us</Link>
            <Link className='text-white py-2 mb-1'>FAQ</Link>
            <Link className='text-white py-2 mb-1'>Contact</Link>
            </div>
          </div>
          <div className='col-2'>
            <h4 className='text-white mb-4'>Quick Links</h4>
            <div className='footer-links d-flex flex-column'>
            <Link className='text-white py-2 mb-1'>PC Sets</Link>
            <Link className='text-white py-2 mb-1'>GPUs</Link>
            <Link className='text-white py-2 mb-1'>CPUs</Link>
            <Link className='text-white py-2 mb-1'>Gaming Builds</Link>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <footer className='py-4'>
      <div className='container-xxl'>
        <div className='row'>
          <div className='col-12'>
            <p className='text-center mb-0 text-white'>&copy; {new Date().getFullYear()} Powered by Ayei Tech Solutions</p>
          </div>
        </div>
      </div>
    </footer>
  </>
};

export default Footer;