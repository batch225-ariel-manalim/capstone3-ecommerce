import { Card, CardImg } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Button, message } from 'antd';
import Swal from 'sweetalert2';


export default function ProductCard(product) {
  const { name, description, price, qty, _id } = product.productProp;
  const { user, setUser } = useContext(UserContext);

  const retrieveUser = (token) => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: { Authorization: `Bearer ${token}` }
    })
      .then(res => res.json())
      .then(data => {
        setUser({ id: data.id, isAdmin: data.isAdmin });
      })
  }
  const buyNow = () => {
    if (user.isAdmin) {
      Swal.fire({
        title: 'Sorry! Admin not allowed!',
        icon: 'error',
        text: 'Only users can placed an order'
      });

    } else if (user.id === null) {
      Swal.fire({
        title: 'Oops! You need to login First',
        icon: 'error',
      });
    }

    else {
      window.location.href = `/checkout/${_id}`;
    }
  }

  return (
    <>
      <Card className='home-wrapper-2 py-2 card-wrapper-hover w-100'>
        <Card.Body>
          <div className="container-xxl">
            <div className="row">
              <div className="col-4"></div>
              <div className="col-8">
                <Card.Title><Link to={`/view/${_id}`} >{name} </Link></Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PHP {price}</Card.Text>
                <Card.Subtitle>Available</Card.Subtitle>
                <Card.Text>{qty} left</Card.Text>
                <Button type="primary" shape='round' onClick={buyNow}>Check Out</Button>
              </div>
            </div>
          </div>

        </Card.Body>
      </Card>

    </>
  )
}

