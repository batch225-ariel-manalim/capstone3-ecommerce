import { NavLink, Link } from 'react-router-dom';
import { BsSearch, BsCart3 } from 'react-icons/bs';
import { ImUser, ImHome } from 'react-icons/im';
import { TiContacts } from 'react-icons/ti';
import { HiShoppingBag } from 'react-icons/hi';
import Marquee from "react-fast-marquee";
import { useContext } from 'react'
import UserContext from '../UserContext';

const Header = () => {
    const { user } = useContext(UserContext)
    return (
        <>
            <header className='header-top-strip py-3'>
                <div className='container-xxl'>
                    <div className='row'>
                        <div className='col-8'>
                            <p className="text-white mb-0 mt-2">
                               <span className='logo-header-color'> Email: </span><a href='mailto:arielmanalim2022@gmail.com' className='mb-0 text-white header-hover-color'>arielmanalim2022@gmail.com</a> | | <span className='logo-header-color'>Mobile:</span>  <a className="text-white header-hover-color" href="tel:+639550374515"> +639550374515</a>
                            </p>
                        </div>
                        <div className="col-4 header-upper-links d-flex justify-content-between header-top-icons">
                            <div>
                                <Link to='/' className='d-flex align-items-center text-white gap-2  header-hover-color'>
                                    <ImHome className='react-icons-size' />
                                    <p className='mb-0'>
                                        Home
                                    </p>
                                </Link>
                            </div>
                            <div>
                                <Link to='/product' className='d-flex align-items-center gap-1 text-white header-hover-color'>
                                    <HiShoppingBag className='react-icons-size' />
                                    <p className='mb-0 '>
                                        Products
                                    </p>
                                </Link>
                            </div>
                            <div>
                                <Link className='d-flex align-items-center gap-1 text-white  header-hover-color' to="/contact">
                                    <TiContacts className='react-icons-size' />
                                    <p className='mb-0'>
                                        Contact
                                    </p>
                                </Link>
                            </div>
                            <div>
                                {(user.id) ?
                                    <NavLink to="/logout" className='d-flex align-items-center gap-1 text-white header-hover-color'>
                                        <ImUser className='react-icons-size' />
                                        <p className='mb-0 '>
                                            Sign out
                                        </p>
                                    </NavLink>
                                    :
                                    <NavLink to='/login' className='d-flex align-items-center gap-1 text-white header-hover-color'>
                                        <ImUser className='react-icons-size' />
                                        <p className='mb-0 '>
                                            Sign in
                                        </p>
                                    </NavLink>
                                }
                            </div>
                        </div>

                    </div>

                </div>
            </header>
            <header className='header-upper py-3'>
                <div className='container-xxl'>
                    <div className='row align-items-center'>
                        <div className='col-2 mb-0'>
                            <h2>
                                <NavLink to='/' className='text-white header-hover-color logo-header-font'><span className='logo-header-color'>Ayei</span> <span>Tech</span> </NavLink>
                            </h2>
                        </div>
                        <div className='col-10 header-upper-links d-flex align-items-center justify-content-between gap-20'>
                            <div className="input-group">
                                <input
                                    type="text" class="form-control" placeholder="Search here..." aria-label="Search here..." aria-describedby="basic-addon2" />
                                <span className="input-group-text p-2" id="basic-addon2">
                                    <BsSearch className='react-icon-size' />
                                </span>
                            </div>
                            <div className='header-hover-color'>
                                <Link className='d-block align-items-center '>
                                    <div className='d-flex flex-column mt-0'>
                                        <span className='badge'>0</span>
                                    </div>
                                    <BsCart3 className='logo-header-color react-icons-size' />
                                </Link>
                            </div>

                        </div>
                    </div>
                </div>
            </header>
            <header className='header-bottom py-3'>

                <Marquee className='d-flex text-white' gradient={false}>
                    <h5 className='mb-0'>
                        Free Shipping Nationwide <b>|</b>   Free Return Policy <b>|</b>   Affordalble prices <b>|</b>   Daily Surprise Deals <b>|</b>  Free Build on Computer Set Purchase <b>|</b>  Free Up to P500 voucher for new shoppers
                    </h5>
                </Marquee>

            </header>
        </>
    );
};

export default Header;