import Meta from '../components/Meta';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined
} from '@ant-design/icons';
import { Layout, Menu, theme } from 'antd';
import React, { useState } from 'react';
import { Link, Outlet, useNavigate } from 'react-router-dom';
import { RiDashboardLine } from 'react-icons/ri';
import { BiAddToQueue, BiListOl, BiEdit, BiLogOut } from 'react-icons/bi';
import { FaShoppingCart, FaShoppingBag } from 'react-icons/fa';
import { FiLogOut } from 'react-icons/fi';
import {VscCheckAll} from 'react-icons/vsc'
import {BsCartCheckFill, BsFillArchiveFill} from 'react-icons/bs'
const { Header, Sider, Content } = Layout;

const Admin = () => {
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const navigate = useNavigate();
  return (
    <>
    <Meta title={"Admin Dashboard"} />
      <Layout>
        <Sider trigger={null} collapsible collapsed={collapsed}>
          <div className="logo">
            <h2 className='text-center text-white fs-5 pt-3 pb-2'>
              <Link to='/' title='Main Homepage'>
              <span className='sm-logo'>AT</span>
              <span className='lg-logo'>Ayei Tech</span>
              </Link>
            </h2>
          </div>
          <Menu
            theme="dark"
            mode="inline"
            defaultSelectedKeys={['none']}
            onClick={({ key }) => {
              if (key == "signout") {

              } else {
                navigate(key);
              }
            }}
            items={[
              {
                className: 'side-bg',
                key: '',
                icon: <RiDashboardLine className="fs-4" />,
                label: 'Dashboard',
              },
              {
                key: 'Product',
                icon: <FaShoppingCart className="fs-4" />,
                label: 'Product',
                children: [
                  {
                    key: '',
                    icon: <BiListOl className="fs-4" />,
                    label: 'Product List',
                    children: [
                      {
                        key: 'allProducts',
                        icon: <VscCheckAll className="fs-4" />,
                        label: 'All Products',
                      },
                      {
                        key: 'activeProducts',
                        icon: <BsCartCheckFill className="fs-4" />,
                        label: 'Active',
                      },
                      {
                        key: 'archivedProducts',
                        icon: <BsFillArchiveFill className="fs-4" />,
                        label: 'Archived',
                      },
                    ]
                  },
                  {
                    key: 'addProduct',
                    icon: <BiAddToQueue className="fs-4" />,
                    label: 'Add Product',
                  },
                ]
              },
              {
                key: 'allOrders',
                icon: <FaShoppingBag className="fs-4" />,
                label: 'Orders',
              },

            ]}
          />
        </Sider>
        <Layout className="site-layout">
          <Header
            className='d-flex justify-content-between ps-1 pe-5'
            style={{
              padding: 0,
              background: colorBgContainer,
            }}
          >
            {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: () => setCollapsed(!collapsed),
            })}

            <div className='d-flex gap-3 align-items-center'>
              <div></div>
              <div className='d-flex gap-3 align-items-center'>
                <div>
                  <img src="/images/64x64.png" alt="adminPicture" />
                </div>
                <div>
                  <h5 className='mb-0'>Ariel M.</h5>
                  <p className='mb-0'>arielmanalim2022@gmail.com</p>
                </div>
                <div>
                  <Link className='dash-btn' to='/logout' title='Sign Out'>
                    <FiLogOut className="fs-4" />
                  </Link>
                </div>
              </div>
            </div>
          </Header>
          <Content
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
              // background: colorBgContainer,
            }}
          >
            <Outlet />
          </Content>
        </Layout>
      </Layout>

    </>
  )
}

export default Admin