import './App.css';
import { useState, useEffect } from 'react'
import { UserProvider } from './UserContext'
import { BrowserRouter, Routes, Route} from 'react-router-dom';
import Layout from './components/Layout';
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';
import Login from './pages/Login';
import Register from './pages/Register';
import OurStore from './pages/OurStore';
import ForgotPassword from './pages/ForgotPassword';
import ResetPassword from './pages/ResetPassword';
import TermsAndConditions from './pages/TermsAndConditions';
import Logout from './pages/Logout';
import Addproduct from './pages/AddProduct';
import AdminLayout from './components/AdminLayout';
import AdminDashboard from './pages/AdminDashboard';
import AllProduct from './pages/AllProduct';
import ActiveProducts from './pages/ActiveProducts';
import ArchiveProducts from './pages/archiveProducts';
import EditProduct from './pages/EditProduct';
import SingleProduct from './pages/SingleProduct';
import AllOrders from './pages/ALlOrders';
import ViewProduct from './pages/ViewProduct';
import Checkout from './pages/Checkout';
import UserOrders from './pages/UserOrders'


function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });
  const unsetUser = () => {
    localStorage.clear()
  }
  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
      .then(res => res.json())
      .then(data => {
        if (typeof data.id !== "undefined") {
          setUser({
            id: data.id,
            isAdmin: data.isAdmin
          });
        } else {
          setUser({
            id: null,
            isAdmin: null
          });
        }
      })
  }, []);

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <BrowserRouter>
          <Routes>
              <Route path='/' element={<Layout />}>
                <Route index element={<Home />} />
                <Route path='about' element={<About />} />
                <Route path='contact' element={<Contact />} />
                <Route path='product' element={<OurStore />} />
                <Route path='tos' element={<TermsAndConditions />} />
                <Route path="logout" element={<Logout />} />
                <Route path='view/:id' element={<ViewProduct/>}/>
                <Route path='checkout/:id' element={<Checkout/>}/>
                <Route path='orders' element={<UserOrders/>} />
              </Route>
              <Route path='/login' element={<Login />} />
              <Route path='/register' element={<Register />} />
              <Route path='/forgot-password' element={<ForgotPassword />} />
              <Route path='/reset-password' element={<ResetPassword />} />
              <Route path='/admin' element={<AdminLayout />} >
                <Route index element={<AdminDashboard />} />
                <Route path='allProducts' element={<AllProduct />} />
                <Route path='activeProducts' element={<ActiveProducts />} />
                <Route path='archivedProducts' element={<ArchiveProducts />} />
                <Route path='addProduct' element={<Addproduct />} />
                <Route path='editProduct/:id' element={<EditProduct />} />
                <Route path='view/:id' element={<SingleProduct />} />
                <Route path='allOrders' element={<AllOrders />} />
              </Route>
          </Routes>
        </BrowserRouter>
      </UserProvider>
    </>
  )
}

export default App;
