import { useState, useEffect, useContext } from 'react';
import { Container, Card, Row, Col } from 'react-bootstrap';
import { useParams } from 'react-router-dom';
import UserContext from '../UserContext';
import { Button, InputNumber } from 'antd';
import Swal from 'sweetalert2';

export default function ViewProduct() {
  const { id } = useParams();
  const { user } = useContext(UserContext);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [qty, setQty] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then(response => response.json())
      .then(result => {
        setName(result.name);
        setDescription(result.description);
        setPrice(parseInt(result.price));
        setQty(parseInt(result.qty));
      })
      .catch(error => console.error(error));
  }, [id]);

  useEffect(() => {
    setTotalPrice(price * quantity);
  }, [price, quantity]);

  const handleQuantityChange = (value) => {
    setQuantity(value);
  };

  const handleBuyNowClick = () => {
    if (user.isAdmin) {
      Swal.fire({
        title: 'Sorry! Admin not allowed!',
        text: 'Only users can place an order',
        icon: 'error',
      });
    } else if (user.id === null) {
      // Swal.fire({
      //   title: 'Oops! You need to login First',
      //   icon: 'error',
      // });
      window.location.href = `/login`;
    }
     else {
      window.location.href = `/checkout/${id}`;
    }
  };

  return (
    <Container className="py-5">
      <Row>
        <Col lg={{ span: 6, offset: 3 }}>
          <Card>
            <Card.Body>
              <Card.Title className="text-center">{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>Php {price}</Card.Text>
              <Button type="primary" shape="round" onClick={handleBuyNowClick}>
                Buy Now
              </Button>
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
