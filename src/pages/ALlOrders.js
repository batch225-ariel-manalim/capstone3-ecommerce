import React from 'react';
import Meta from '../components/Meta';
import Loading from '../components/Loading';
import { useEffect, useState } from 'react';
import { Button, Space, Table, message } from 'antd';

const AllOrders = () => {
    const [filteredInfo, setFilteredInfo] = useState({});
    const [sortedInfo, setSortedInfo] = useState({});
    const [isLoading, setIsLoading] = useState(true);
    const [products, setProducts] = useState([]);
    const handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter);
        setFilteredInfo(filters);
        setSortedInfo(sorter);
    };

    const columns = [
        {
            title: 'Order ID',
            dataIndex: 'Order ID',
            key: 'Order ID',
            ellipsis: true,
        },
        {
            title: 'Customer Name',
            dataIndex: 'Customer Name',
            key: 'Customer Name',
            // sorter: (a, b) => a.description.localeCompare(b.description),
            // sortOrder: sortedInfo.columnKey === 'description' ? sortedInfo.order : null,
            ellipsis: true,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            // sorter: (a, b) => a.name.localeCompare(b.name),
            // sortOrder: sortedInfo.columnKey === 'name' ? sortedInfo.order : null,
            ellipsis: true,
        },
        {
            title: 'Quantity',
            dataIndex: 'Quantity',
            key: 'Quantity',
            ellipsis: true,
        },
        {
            title: 'Total Amount',
            dataIndex: 'Total Amount',
            key: 'Total Amount',
            ellipsis: true,
        },
        {
            title: 'Status',
            dataIndex: 'isActive',
            key: 'isActive',
            render: (isActive) => {
                return (
                    isActive ?
                        <span style={{ color: 'green' }}><i className="fas fa-circle"></i> Completed</span> :
                        <span style={{ color: 'red' }}><i className="fas fa-circle"></i> Incomplete</span>
                )
            }
        },
        {
            className: 'text-center',
            title: 'Actions',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    {
                        record.isActive ?
                        <Button danger className='red-button' shape='round'>Complete</Button>
                        :
                        <Button className='green-button' shape='round' disabled>Complete</Button>
                    }
                    <Button type="primary" shape='round' href={`/admin/view/${record._id}`}>Details</Button>
                </Space>
            ),
        },
    ];

    return (
        isLoading ?
            <Loading />
            :
            <>
                <Meta title={"Admin Dashboard - Archived Products"} />
                <h1 className='dash-text-color'>Archived Products</h1>
                <Table className='py-4' columns={columns} dataSource={products} onChange={handleChange} />
            </>
    );
};

export default AllOrders;
