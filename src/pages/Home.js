import React, { useEffect } from 'react';
import ProductCard from '../components/ProductCard';
import Carousel from 'react-bootstrap/Carousel';
import { Link } from 'react-router-dom';

const Home = () => {
  return (
    <>
      <section className="home-wrapper-3 py-5">
        <div className="container-xxl">
          <div className="row">
            <Link to='/product' className="col-8">
              <Carousel className="main-banner position-relative">
                <Carousel.Item interval={3000}>
                  <img
                    className="rounded-3"
                    src="images/main-banner.jpg"
                    alt="First slide"
                  />
                </Carousel.Item>
                <Carousel.Item interval={3000}>
                  <img
                    className="rounded-3"
                    src="images/main-banner-1.jpg"
                    alt="Second slide"
                  />
                </Carousel.Item>
                <Carousel.Item interval={3000}>
                  <img
                    className="rounded-3"
                    src="images/main-banner-2.jpg"
                    alt="Third slide"
                  />
                </Carousel.Item>
                <Carousel.Item interval={3000}>
                  <img
                    className="rounded-3"
                    src="images/main-banner-3.jpg"
                    alt="Third slide"
                  />
                </Carousel.Item>
              </Carousel>
            </Link>
            <div className='col-4'>
              <div className='d-flex flex-wrap gap-10 justify-content-between'>
                <Link to='/product' className='small-banner position-relative'>
                  <img src='images/catbanner-01.jpg' className='rounded-3' alt='main-banner' />
                </Link>
                <Link to='/product' className='small-banner position-relative'>
                  <img src='images/catbanner-02.jpg' className='rounded-3' alt='main-banner' />
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section className='home-wrapper-2 py-5'>
        <div className='container-xxl'>
          <div className='row'>
            <div className='col-12'>
              <h3 className='section-heading'>Shop By Category</h3>
              <div className='categories d-flex justify-content-between flex-wrap align-items-center'>
                <div className='d-flex align-items-center product-card'>
                  <div>
                    <h6>CPU Cases</h6>
                    <p>9 Items left</p>
                  </div>
                  <div className='product-image'>
                    <img src='images/computer-set.jpg' alt='computer-set' />
                    <img src='images/computer-set2.jpg' alt='computer-set' />
                  </div>
                </div>
                <div className='d-flex align-items-center product-card'>
                  <div>
                    <h6>Processors</h6>
                    <p>17 Items left</p>
                  </div>
                  <div className='product-image'>
                    <img src='images/proce2.jpg' alt='Processors' />
                    <img src='images/proce.jpg' alt='Processors' />
                  </div>
                </div>
                <div className='d-flex align-items-center product-card'>
                  <div>
                    <h6>GPUs</h6>
                    <p>5 Items left</p>
                  </div>
                  <div className='product-image'>
                    <img src='images/GPU.jpg' alt='gpu' />
                    <img src='images/GPU2.jpg' alt='gpu' />
                  </div>
                </div>
                <div className='d-flex align-items-center product-card'>
                  <div>
                    <h6>Power Supplies</h6>
                    <p>10 Items left</p>
                  </div>
                  <div className='product-image'>
                    <img src='images/powerSupply.jpg' alt='psu' />
                    <img src='images/powerSupply2.jpg' alt='psu' />
                  </div>
                </div>
                <div className='d-flex align-items-center product-card'>
                  <div>
                    <h6>RAMs</h6>
                    <p>22 Items left</p>
                  </div>
                  <div className='product-image'>
                    <img src='images/ram.jpg' alt='ram' />
                    <img src='images/ram2.jpg' alt='ram' />
                  </div>
                </div>
                <div className='d-flex align-items-center product-card'>
                  <div>
                    <h6>SSDs</h6>
                    <p>37 Items left</p>
                  </div>
                  <div className='product-image'>
                    <img src='images/ssd.jpg' alt='ssd' />
                    <img src='images/ssd2.jpg' alt='ssd' />
                  </div>
                </div>
                <div className='d-flex align-items-center product-card'>
                  <div>
                    <h6>Motherboards</h6>
                    <p>17 Items left</p>
                  </div>
                  <div className='product-image'>
                    <img src='images/mobo.jpg' alt='mobo' />
                    <img src='images/mobo2.jpg' alt='mobo' />
                  </div>
                </div>
                <div className='d-flex align-items-center product-card'>
                  <div>
                    <h6>Fans</h6>
                    <p>14 Items left</p>
                  </div>
                  <div className='product-image'>
                    <img src='images/fans.jpg' alt='camera' />
                    <img src='images/fans2.jpg' alt='camera' />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
     
    </>
  )
};

export default Home