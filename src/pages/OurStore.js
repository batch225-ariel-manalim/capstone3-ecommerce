import ProductCard from '../components/ProductCard';
import React, { useState, useEffect } from 'react';
import Meta from '../components/Meta';
import Loading from '../components/Loading';


export default function OurStore() {
  const [grid, setGrid] = useState(4);
  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect((isLoading) => {

    // If it detects the data coming from fetch, the set isloading going to be false
    fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`)
        .then(response => response.json())
        .then(result => {
            setProducts(
                result.map(product => {
                    return (
                        <ProductCard key={product._id} productProp={product} />               
                    )
                })
            )

            // Sets the loading state to false
            setIsLoading(false)
        })
}, [])
  return (
    <>
      <Meta title={"Products"} />
      <div className="store-wrapper home-wrapper-2 py-5">
        <div className="container-xxl">
          <div className="row">
            <div className="col-3">
              <div className='filter-card mb-3'>
                <h3 className="filter title">Shop By Categories</h3>
                <div>
                  <ul>
                    <li>CPU Cases</li>
                    <li>Processors</li>
                    <li>GPUs</li>
                    <li>Power Supplies</li>
                    <li>Rams</li>
                    <li>SSDs</li>
                    <li>Motherboards</li>
                    <li>Fans</li>
                  </ul>
                </div>
              </div>
              <div className='filter-card mb-3'>
                <h3 className="filter title">Filter By</h3>
                <div>
                  <h5 className="sub-title">Availability</h5>
                  <div>
                    <div className="form-check">
                      <input
                        className='form-check-input'
                        type="checkbox"
                        value=""
                        id=''
                      />
                      <label className='form-check-label' htmlFor="">In Stock (1)</label>
                    </div>
                    <div className="form-check">
                      <input
                        className='form-check-input'
                        type="checkbox"
                        value=""
                        id=''
                      />
                      <label className='form-check-label' htmlFor="">Out of Stock (0)</label>
                    </div>
                  </div>
                  <h5 className="sub-title">Price</h5>
                  <div className='d-flex align-items-center gap-10'>
                    <div className="form-floating">
                      <input
                        type="email"
                        className="form-control"
                        id="floatingInput"
                        placeholder="From"
                      />
                      <label htmlFor="floatingInput">From</label>
                    </div>
                    <div className="form-floating">
                      <input
                        type="email"
                        className="form-control"
                        id="floatingInput1"
                        placeholder="To"
                      />
                      <label htmlFor="floatingInput1">To</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-9">
              <div className="products-list pb-5">
                <div className="d-flex gap-10 flex-wrap">
                  {isLoading ?
                    <Loading />
                    :
                    <>
                      {products}
                    </>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}