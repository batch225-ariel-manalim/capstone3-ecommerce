import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Meta from '../components/Meta';
import { Link } from 'react-router-dom';
import Typed from "react-typed";

export default function Register() {
    const { user, setUser } = useContext(UserContext);
    const navigate = useNavigate();
    const [name, setName] = useState('');
    const [mobile, setMobile] = useState('');
    const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('')
    const [isActive, setIsActive] = useState(false);

    function register(event) {
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(response => response.json())
            .then(result => {
                if (result === true) {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: 'Email already exist!'
                    })
                } else {
                    fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({
                            name: name,
                            mobile: mobile,
                            email: email,
                            password: password1
                        })
                    })
                        .then(response => response.json())
                        .then(result => {
                            console.log(result);
                            setEmail('');
                            setPassword1('');
                            setPassword2('');
                            setName('');
                            setMobile('');

                            if (result) {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: 'Registered Successfully',
                                    text: "Thank you",
                                    showConfirmButton: false,
                                    timer: 1500
                                })

                                navigate('/login')

                            } else {
                                Swal.fire({
                                    title: 'Registration Failed',
                                    icon: 'error',
                                    text: "Try again!"
                                })
                            }
                        })
                }
            })

    }
    useEffect(() => {
        if ((name !== '' && mobile.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [name, mobile, email, password1, password2])

    return (
        (user.id !== null) ?
            <Navigate to="/login" />
            :
            <>
                <Meta title={"Register"} />
                <section className="login-main position-relative">
                    <img src="images/bg-main.jpg" className='img-fluid' alt="background-img" />
                    <div className='container-xxl main-login position-absolute'>
                        <div className='row'>
                            <div className='col-8'>
                                <div className='position-relative'>
                                    <div className='position-absolute'>
                                        <div className="type-effect mb-0">
                                            <Typed
                                                strings={[
                                                    'Affordable prices are waiting for you...',
                                                    "Free Shipping Natiowide",
                                                    'Customize your PC at your own pace now.'
                                                ]}
                                                typeSpeed={50}
                                                backSpeed={15}
                                                loop
                                            />
                                        </div>
                                        <h1 className='text-white'>Build Your <br />dream <br />Computer now</h1>
                                        <Link className='link-button' to='/'>Main Page</Link>
                                    </div>
                                </div>
                            </div>
                            <div className="col-4 login-wrapper d-block justify-content-between flex-wrap align-items-center">
                                <div className="auth-card register-card">
                                    <h2 className='mb-3'>Sign up</h2>
                                    <p className='login-text-color'>
                                        Already a user? <a className='login-links' href="/login">Login now</a>
                                    </p>
                                    <Form className='filter-form d-flex flex-column gap-15' onSubmit={event => register(event)}>
                                        <Form.Group controlId="name">
                                            <Form.Label className='login-text-color'>Full Name</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={name}
                                                onChange={event => setName(event.target.value)}
                                                className='form-control'
                                                required
                                            />
                                        </Form.Group>
                                        <Form.Group controlId="mobile">
                                            <Form.Label className='login-text-color'>Mobile Number</Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={mobile}
                                                onChange={event => setMobile(event.target.value)}
                                                className='form-control'
                                                required
                                            />
                                        </Form.Group>
                                        <Form.Group controlId="email">
                                            <Form.Label className='login-text-color'>Email</Form.Label>
                                            <Form.Control
                                                type="email"
                                                value={email}
                                                onChange={event => setEmail(event.target.value)}
                                                className='form-control'
                                                required
                                            />
                                        </Form.Group>
                                        <Form.Group className='mt-1' controlId="password1">
                                            <Form.Label className='login-text-color'>Password</Form.Label>
                                            <Form.Control
                                                type="password"
                                                value={password1}
                                                onChange={event => setPassword1(event.target.value)}
                                                className='form-control'
                                                required />
                                        </Form.Group>
                                        <Form.Group className='mt-1' controlId="password2">
                                            <Form.Label className='login-text-color'>Verify Password</Form.Label>
                                            <Form.Control
                                                type="password"
                                                value={password2}
                                                onChange={event => setPassword2(event.target.value)}
                                                className='form-control'
                                                required />
                                        </Form.Group>
                                        <div className="form-check mt-2">
                                            <input
                                                className='form-check-input'
                                                type="checkbox"
                                                value=""
                                                id=''
                                                required
                                            />
                                            <label className='form-check-label login-text-color'>By registering I agree with the <a href="/tos">Terms and Conditions</a></label>
                                        </div>
                                        <div className='d-flex justify-content-center gap-15 align-items-center'>
                                        </div>
                                        {isActive ?
                                            <Button className='login-wide-button my-2' variant="danger" type="submit" id="submitBtn">
                                                Sign up
                                            </Button>
                                            :
                                            <Button className='login-wide-button my-2' variant="danger" type="submit" id="submitBtn" disabled>
                                                Sign up
                                            </Button>
                                        }
                                    </Form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section >
            </>
    )
}