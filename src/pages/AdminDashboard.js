import React from "react";
import { BsArrowDownRight, BsArrowUpRight } from "react-icons/bs";
import { Column } from "@ant-design/plots";
import { Table } from "antd";
const columns = [
  {
    title: "SNo",
    dataIndex: "key",
  },
  {
    title: "Name",
    dataIndex: "name",
  },
  {
    title: "Product",
    dataIndex: "product",
  },
  {
    title: "Status",
    dataIndex: "staus",
  },
];
const Dashboard = () => {
  const data = [
    {
      type: "Jan 2023",
      sales: 70,
    },
    {
      type: "Feb 2023",
      sales: 90,
    },
    {
      type: "Mar 2022",
      sales: 61,
    },
    {
      type: "Apr 2022",
      sales: 145,
    },
    {
      type: "May 2022",
      sales: 48,
    },
    {
      type: "Jun 2022",
      sales: 90,
    },
    {
      type: "July 2022",
      sales: 30,
    },
    {
      type: "Aug 2022",
      sales: 100,
    },
    {
      type: "Sept 2022",
      sales: 40,
    },
    {
      type: "Oct 2022",
      sales: 60,
    },
    {
      type: "Nov 2022",
      sales: 50,
    },
    {
      type: "Dec 2022",
      sales: 38,
    },
  ];
  const config = {
    className: "dash-text-color",
    data,
    xField: "type",
    yField: "sales",
    color: ({ type }) => {
      return "#ffa41c";
    },
    label: {
      position: "middle",
      style: {
        fill: "#FFFFFF",
        opacity: 1,
      },
    },
    xAxis: {
      label: {
        autoHide: true,
        autoRotate: false,
      },
    },
    meta: {
      type: {
        alias: "Month",
      },
      sales: {
        alias: "Income",
      },
    },
  };
  return (
    <div>
      <h1 className="mb-4 title dash-text-color">Dashboard</h1>
      <div className="dash-title d-flex justify-content-between align-items-center gap-3 dash-text-color">
        <div className="d-flex justify-content-between align-items-end flex-grow-1 bg-white p-3 rounded-3 dash-color">
          <div>
            <h3 className="desc">Total Sales</h3>
            <h4 className="mb-0 sub-title dash-text-color">&#8369;37,000</h4>
          </div>
          <div className="d-flex flex-column align-items-end">
            <h6 className="green">
              <BsArrowUpRight /> 27%
            </h6>
            <p className="mb-0 desc">Compared Last Month</p>
          </div>
        </div>
        <div className="d-flex justify-content-between align-items-end flex-grow-1 bg-white p-3 rounded-3 dash-color">
          <div>
            <h3 className="desc">Total Orders</h3>
            <h4 className="mb-0 sub-title dash-text-color">27</h4>
          </div>
          <div className="d-flex flex-column align-items-end">
            <h6 className="red">
              <BsArrowDownRight /> 7%
            </h6>
            <p className="mb-0  desc">Compared last Month</p>
          </div>
        </div>
        <div className="d-flex justify-content-between align-items-end flex-grow-1 bg-white p-3 rounded-3 dash-color">
          <div>
            <h3 className="desc">Advertising Spend</h3>
            <h4 className="mb-0 sub-title dash-text-color">&#8369;1,346</h4>
          </div>
          <div className="d-flex flex-column align-items-end">
            <h6 className="red">
              <BsArrowUpRight /> 47%
            </h6>
            <p className="mb-0 desc">Compared Last Month</p>
          </div>
        </div>
      </div>
      <div className="mt-4">
        <h3 className="mb-5 title text-white dash-text-color">Income Statistics</h3>
        <div className="pt-4">
          <Column {...config} />
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
