import { useState, useEffect, useContext } from 'react';
import { Container, Card, Row, Col} from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { Button, Space, Table, Image, message } from 'antd';

export default function SingleProduct() {
    const { id } = useParams()
    const { user } = useContext(UserContext)


    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [qty, setQty] = useState("");

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
            .then(response => response.json())
            .then(result => {
                setName(result.name)
                setDescription(result.description)
                setPrice(result.price)
                setQty(result.qty)
            })
    }, [id])
    return (
        <>
            <Container className="mt-5">
                <Row>
                    <Col lg={{ span: 6, offset: 3 }}>
                        <Card>
                            <Card.Body className="text-center">
                                <Card.Title>{name}</Card.Title>
                                <Card.Subtitle>Description:</Card.Subtitle>
                                <Card.Text>{description}</Card.Text>
                                <Card.Subtitle>Price:</Card.Subtitle>
                                <Card.Text>PhP {price}</Card.Text>
                                <Card.Subtitle>Inventory</Card.Subtitle>
                                <Card.Text>{qty}</Card.Text>
                                <Button type="primary" shape='round' href={`/admin/editProduct/${id}`}>Edit</Button>
                                    
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
        </>
    )
}