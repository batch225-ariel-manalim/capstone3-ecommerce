import { useState, useEffect, useContext } from 'react';
import { Card } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Form, Input, InputNumber, Select } from 'antd';
const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
export default function Checkout() {

  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log('Received values of form: ', values);
  };
  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 70,
        }}
      >
        <Option value="63">+63</Option>
        <Option value="09">09</Option>
      </Select>
    </Form.Item>
  );
  const suffixSelector = (
    <Form.Item name="suffix" noStyle>
      <Select
        style={{
          width: 70,
        }}
      >
        <Option value="USD">$</Option>
        <Option value="CNY">¥</Option>
      </Select>
    </Form.Item>
  );

  const navigate = useNavigate()
  const { id } = useParams();
  const { user } = useContext(UserContext);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [qty, setQty] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);

  const checkout = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/order/checkout`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        products: [
          {
            productId: id,
            quantity: quantity
          }
        ]
      })
    })
      .then(response => response.json())
      .then(result => {
        if (result) {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "You have enrolled successfully!"
          })

          navigate('/product')
        } else {
          console.log(result)

          Swal.fire({
            title: "Something went wrong!",
            icon: "error",
            text: "Please try again :("
          })
        }
      })
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then(response => response.json())
      .then(result => {
        setName(result.name);
        setDescription(result.description);
        setPrice(parseInt(result.price));
        setQty(parseInt(result.qty));
      })
      .catch(error => console.error(error));
  }, [id]);

  useEffect(() => {
    setTotalPrice(price * quantity);
  }, [price, quantity]);

  const handleQuantityChange = (value) => {
    setQuantity(value);
  };

  const handleBuyNowClick = () => {
    if (user.isAdmin) {
      Swal.fire({
        title: 'Sorry! Admin not allowed!',
        text: 'Only users can place an order',
        icon: 'error',
      });
    } else if (user.id === null) {
      Swal.fire({
        title: 'Oops! You need to login First',
        icon: 'error',
      });
    }
    else {
      checkout(id);
    }
  };

  return (
    <div className="container-xxl py-5">
      <div className="row">
        <div className="col-7">
          <Card.Title className="text-center">{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>Php {price}</Card.Text>
        </div>
        <div className="col-5">
          <h3 className='text-center'>Shipping Details</h3>
          <Form
            {...formItemLayout}
            form={form}
            name="register"
            onFinish={onFinish}
            initialValues={{
              residence: ['zhejiang', 'hangzhou', 'xihu'],
              prefix: '+63',
            }}
            style={{
              maxWidth: 600,
            }}
            scrollToFirstError
          >

            <Form.Item
              name="Address"
              label="Address"
              tooltip="Input exact location."
              rules={[
                {
                  required: true,
                  message: 'Please input your address!',
                  whitespace: true,
                },
              ]}
            >
              <Input className='py-2' />
            </Form.Item>

            <Form.Item
              name="phone"
              label="Mobile"
              rules={[
                {
                  required: true,
                  message: 'Please input your phone number!',
                },
              ]}
            >
              <Input
                addonBefore={prefixSelector}
                style={{
                  width: '100%',
                }}
              />
            </Form.Item>

            <Form.Item
              name="intro"
              label="message"
              rules={[
                {
                  required: true,
                  message: 'Please input Intro',
                },
              ]}
            >
              <Input.TextArea showCount maxLength={100} />
            </Form.Item>

            <Form.Item
              name="Payment Method"
              label="MOP"
              rules={[
                {
                  required: true,
                  message: 'Please select Payment Method!',
                },
              ]}
            >
              <Select placeholder="Select Payment Method">
                <Option value="male">Cash on Delivery</Option>
                <Option value="female">E-Wallet</Option>
                <Option value="other">Credit / Debit Card</Option>
              </Select>
            </Form.Item>
          </Form>
          <div className='d-flex flex-column align-items-end'>
            <InputNumber
              addonBefore="Qty"
              min={1}
              max={qty}
              defaultValue={quantity}
              onChange={handleQuantityChange}
              className='mb-3'
            />
            <Card.Text> <b>Total Price:</b> Php {totalPrice}</Card.Text>
            <button className="btn btn-primary" onClick={handleBuyNowClick}>Buy Now</button>
          </div>


        </div>

      </div>
    </div>
  );
}
