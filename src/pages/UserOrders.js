import { useState, useEffect, useContext } from 'react';
import { Card } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Form, Input, InputNumber, Select } from 'antd';
const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};

export default function Checkout() {
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log('Received values of form: ', values);
  };
  const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select
        style={{
          width: 70,
        }}
      >
        <Option value="63">+63</Option>
        <Option value="09">09</Option>
      </Select>
    </Form.Item>
  );
  const suffixSelector = (
    <Form.Item name="suffix" noStyle>
      <Select
        style={{
          width: 70,
        }}
      >
        <Option value="USD">$</Option>
        <Option value="CNY">¥</Option>
      </Select>
    </Form.Item>
  );

  const { id } = useParams();
  const navigate = useNavigate()
  const { user } = useContext(UserContext);
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState(0);
  const [qty, setQty] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);

  const checkout = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/order/checkout`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        products: [
          {
            productId: id,
            quantity: quantity
          }
        ]
      })
    })
      .then(response => response.json())
      .then(result => {
        if (result) {
          Swal.fire({
            title: "Success!",
            icon: "success",
            text: "You have enrolled successfully!"
          })

          navigate('/orders')
        } else {
          console.log(result)

          Swal.fire({
            title: "Something went wrong!",
            icon: "error",
            text: "Please try again :("
          })
        }
      })
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
      .then(response => response.json())
      .then(result => {
        setName(result.name);
        setDescription(result.description);
        setPrice(parseInt(result.price));
        setQty(parseInt(result.qty));
      })
      .catch(error => console.error(error));
  }, [id]);

  useEffect(() => {
    setTotalPrice(price * quantity);
  }, [price, quantity]);

  const handleQuantityChange = (value) => {
    setQuantity(value);
  };

  const handleBuyNowClick = () => {
    if (user.isAdmin) {
      Swal.fire({
        title: 'Sorry! Admin not allowed!',
        text: 'Only registered users can buy products.',
        icon: 'error',
        confirmButtonText: 'OK'
      });
    } else {
      if (qty < 1) {
        Swal.fire({
          title: 'Out of Stock!',
          text: 'This product is out of stock.',
          icon: 'error',
          confirmButtonText: 'OK'
        });
      } else {
        checkout(id);
      }
    }
  };

  return (
    <div className="checkout-container">
      <h1>Checkout</h1>
      <div className="checkout-summary">
        <Card>
          <Card.Body>
            <Card.Title>{name}</Card.Title>
            <Card.Text>{description}</Card.Text>
            <Card.Text>Price: ${price}</Card.Text>
            <Form {...formItemLayout} form={form} name="register" onFinish={onFinish} scrollToFirstError>
              <Form.Item
                name="quantity"
                label="Quantity"
                rules={[
                  {
                    type: 'number',
                    min: 1,
                    max: qty,
                    message: `Please enter a number between 1 and ${ qty }`,
        },
        ]}
              initialValue={quantity}
        >
              <InputNumber onChange={handleQuantityChange} />
            </Form.Item>
            <Form.Item
              name="totalPrice"
              label="Total Price"
              initialValue={totalPrice}
            >
              <Input disabled={true} />
            </Form.Item>
            <Form.Item
              name="firstName"
              label="First Name"
              rules={[
                {
                  required: true,
                  message: 'Please input your first name!',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="lastName"
              label="Last Name"
              rules={[
                {
                  required: true,
                  message: 'Please input your last name!',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="contactNumber"
              label="Contact Number"
              rules={[
                {
                  required: true,
                  message: 'Please input your contact number!',
                },
              ]}
            >
              <Input addonBefore={prefixSelector} addonAfter={suffixSelector} style={{ width: '100%' }} />
            </Form.Item>
            <Form.Item
              name="email"
              label="Email"
              rules={[
                {
                  type: 'email',
                  message: 'The input is not a valid email!',
                },
                {
                  required: true,
                  message: 'Please input your email!',
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="address"
              label="Address"
              rules={[
                {
                  required: true,
                  message: 'Please input your address!',
                },
              ]}
            >
              <Input.TextArea />
            </Form.Item>
          </Form>
          <button className="btn btn-primary" onClick={handleBuyNowClick}>Buy Now</button>
          <Link to="/">Back to Home</Link>
        </Card.Body>
      </Card>
    </div>
        </div >
        );
}