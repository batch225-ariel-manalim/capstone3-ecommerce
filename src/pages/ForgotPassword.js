import React from 'react';
import { useState, useEffect} from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import Meta from '../components/Meta';
import { Link } from 'react-router-dom';
import Typed from "react-typed";

const ForgotPassword = () => {
    const [email, setEmail] = useState('');
    const navigate = useNavigate();
    const [isActive, setIsActive] = useState(false);

    function emailContain(event) {
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(response => response.json())
            .then(result => {
                if (result !== true) {
                    Swal.fire({
                        title: 'Not Found on our Database',
                        icon: 'error',
                        text: 'Try Again!'
                    })
                } else {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Send Successfully',
                        text: "Please Check Your Email",
                        showConfirmButton: false,
                        timer: 3000
                    })

                    navigate('/login')

                }

            })
    }

    useEffect(() => {
        if ((email !== '')) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email])
    return (
        <>
            <Meta title={"Forgot Password"} />
            <section className="login-main position-relative">
                <img src="images/bg-main.jpg" className='img-fluid' alt="background-img" />
                <div className='container-xxl main-login position-absolute'>
                    <div className='row'>
                        <div className='col-8'>
                            <div className='position-relative'>
                                <div className='position-absolute'>
                                    <div className="type-effect mb-0">
                                        <Typed
                                            strings={[
                                                'Affordable prices are waiting for you...',
                                                "Free Shipping Natiowide",
                                                'Customize your PC at your own pace now.'
                                            ]}
                                            typeSpeed={50}
                                            backSpeed={15}
                                            loop
                                        />
                                    </div>
                                    <h1 className='text-white'>Build Your <br />dream <br />Computer now</h1>
                                    <Link className='link-button' to='/'>Main Page</Link>
                                </div>
                            </div>
                        </div>
                        <div className=" col-4 login-wrapper py-5 d-block justify-content-between flex-wrap align-items-center">
                            <div className="auth-card">
                                <h2 className='mb-3'>Reset Password</h2>
                                <p className='login-text-color'>
                                    New user? <a className='login-links' href="/register">Create an Account</a>
                                </p>
                                <Form onSubmit={event => emailContain(event)} className='d-flex flex-column gap-15'>
                                    <Form.Group controlId="email">
                                        <Form.Label className='login-text-color'>Email</Form.Label>
                                        <Form.Control
                                            type="email"
                                            value={email}
                                            onChange={event => setEmail(event.target.value)}
                                            className='form-control mt-2'
                                            required
                                        />
                                    </Form.Group>

                                    <p className='mt-3 mb-3 login-text-color'>
                                        We will send you an email to reset your password
                                    </p>
                                    {isActive ?
                                        <Button className='login-wide-button my-2' variant="danger" type="submit" id="submitBtn">
                                            Send
                                        </Button>
                                        :
                                        <Button className='login-wide-button my-2' variant="danger" type="submit" id="submitBtn" disabled>
                                            Send
                                        </Button>
                                    }
                                    <Link className='text-center' to='/login'>Cancel</Link>
                                </Form>

                            </div>
                        </div>
                    </div>
                </div>
            </section >
        </>
    )
}

export default ForgotPassword