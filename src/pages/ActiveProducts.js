import React from 'react';
import Meta from '../components/Meta';
import Loading from '../components/Loading';
import { useEffect, useState } from 'react';
import { Button, Space, Table, message } from 'antd';

const ActiveProducts = () => {
    const [filteredInfo, setFilteredInfo] = useState({});
    const [sortedInfo, setSortedInfo] = useState({});

    const handleChange = (pagination, filters, sorter) => {
        console.log('Various parameters', pagination, filters, sorter);
        setFilteredInfo(filters);
        setSortedInfo(sorter);
    };

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            sorter: (a, b) => a.name.localeCompare(b.name),
            sortOrder: sortedInfo.columnKey === 'name' ? sortedInfo.order : null,
            ellipsis: true,
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
            sorter: (a, b) => a.description.localeCompare(b.description),
            sortOrder: sortedInfo.columnKey === 'description' ? sortedInfo.order : null,
            ellipsis: true,
        },
        {
            title: 'Price',
            dataIndex: 'price',
            key: 'price',
            sorter: (a, b) => a.price - b.price,
            sortOrder: sortedInfo.columnKey === 'price' ? sortedInfo.order : null,
            ellipsis: true,
        },
        {
            title: 'Status',
            dataIndex: 'isActive',
            key: 'isActive',
            render: (isActive) => {
                return (
                    isActive ?
                        <span style={{ color: 'green' }}><i className="fas fa-circle"></i> Active</span> :
                        <span style={{ color: 'red' }}><i className="fas fa-circle"></i> Inactive</span>
                )
            }
        },
        {
            className: 'text-center',
            title: 'Actions',
            key: 'action',
            render: (text, record) => (
                <Space size="middle">
                    {
                        record.isActive ?
                            <Button danger className='red-button' shape='round' onClick={() => archiveProduct(record._id)}>Archive</Button> :
                            <Button className='green-button' shape='round' onClick={() => activateProduct(record._id)}>Activate</Button>
                    }
                    <Button type='primary' shape='round' href={`/admin/view/${record._id}`}>Details</Button>
                </Space>
            ),
        },
    ];

    const [products, setProducts] = useState([]);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/activeProducts`) // only fetch active products
            .then(response => response.json())
            .then(result => {
                setProducts(result);
                setIsLoading(false);
            })
    }, []);
    const archiveProduct = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                isActive: false,
            }),
        }).then(() => {
            // update the products array to reflect the updated state of the product
            setProducts(products.map((product) => {
                if (product._id === id) {
                    return {
                        ...product,
                        isActive: false,
                    };
                }
                return product;
            }));
            message.success("Archived Successfully")
        })
    };
    const activateProduct = (id) => {
        fetch(`${process.env.REACT_APP_API_URL}/products/activate/${id}`, {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            isActive: true,
          }),
        }).then(() => {
          // update the products array to reflect the updated state of the product
          setProducts(products.map((product) => {
            if (product._id === id) {
              return {
                ...product,
                isActive: true,
              };
            }
            // message.success(`${product.name} activated successfully`)
            console.log(product)
            return product;
          }));
          message.success("Product Activated!")
        });
      };
    return (
        isLoading ?
            <Loading />
            :
            <>
                <Meta title={"Admin Dashboard - Active Products"} />
                <h1 className='dash-text-color'>Active Products</h1>
                <Table className='py-4' columns={columns} dataSource={products} onChange={handleChange} />
            </>
    );
};

export default ActiveProducts;
