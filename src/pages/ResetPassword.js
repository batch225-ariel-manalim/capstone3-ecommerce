import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Meta from '../components/Meta';
import { Link } from 'react-router-dom';
import Typed from "react-typed";

const ResetPassword = () => {
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [isActive, setIsActive] = useState(false);
    useEffect(() => {
        if ((password1 !== '' && password2 !== '')&&(password1 === password2)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [password1, password2])
    return (
        <>
            <Meta title={"Reset Password"} />
            <section className="login-main position-relative">
                <img src="images/bg-main.jpg" className='img-fluid' alt="background-img" />
                <div className='container-xxl main-login position-absolute'>
                    <div className='row'>
                        <div className='col-8'>
                            <div className='position-relative'>
                                <div className='position-absolute'>
                                    <div className="type-effect mb-0">
                                        <Typed
                                            strings={[
                                                'Affordable prices are waiting for you...',
                                                "Free Shipping Natiowide",
                                                'Customize your PC at your own pace now.'
                                            ]}
                                            typeSpeed={50}
                                            backSpeed={15}
                                            loop
                                        />
                                    </div>
                                    <h1 className='text-white'>Build Your <br />dream <br />Computer now</h1>
                                    <Link className='link-button' to='/'>Main Page</Link>
                                </div>
                            </div>
                        </div>
                        <div className=" col-4 login-wrapper py-5 d-block justify-content-between flex-wrap align-items-center">
                            <div className="auth-card">
                                <h2 className='mb-3'>Reset Password</h2>
                                <p className='login-text-color'>
                                    New user? <a className='login-links' href="/register">Create an Account</a>
                                </p>
                                <form action="" className='d-flex flex-column gap-15'>
                                    <div>
                                        <label htmlFor="password1" className='login-text-color'>New Password</label>
                                        <input
                                            type="password"
                                            name='password'
                                            value={password1}
                                            onChange={event => setPassword1(event.target.value)}
                                            className='form-control mt-2'
                                            required
                                        />
                                    </div>
                                    <div>
                                        <label htmlFor="password2" className='login-text-color'>Verify Password</label>
                                        <input
                                            type="password"
                                            name='password'
                                            value={password2}
                                            onChange={event => setPassword2(event.target.value)}
                                            className='form-control mt-2'
                                            required
                                        />
                                    </div>
                                    <div className='d-flex justify-content-center gap-15 align-items-center'>
                                        {isActive ?
                                            <Button className='login-wide-button my-2' variant="danger" type="submit" id="submitBtn">
                                                Send
                                            </Button>
                                            :
                                            <Button className='login-wide-button my-2' variant="danger" type="submit" id="submitBtn" disabled>
                                                Send
                                            </Button>
                                        }
                                    </div>
                                    <Link className='text-center' to='/login'>Cancel</Link>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section >
        </>
    )
}

export default ResetPassword