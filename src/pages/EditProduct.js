import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';
import Meta from '../components/Meta';

const EditProduct = () => {
    const { id } = useParams()
    const navigate = useNavigate();
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    // const [image, setImage] = useState('');
    const [qty, setQty] = useState('');
    const [isActive, setIsActive] = useState(false);

    const [name1, setName1] = useState("");
    const [description1, setDescription1] = useState("");
    const [price1, setPrice1] = useState(0);
    const [qty1, setQty1] = useState("");

    function editProduct(event) {
        event.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/products/update/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                qty: qty,
                // image: image
            })
        })
            .then(response => response.json())
            .then(result => {
                setName('');
                setDescription('');
                setPrice('');
                setQty('');
                // setImage('');

                if (result) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Product Updated Successfully',
                        showConfirmButton: false,
                        timer: 1500
                    })

                    navigate('/admin/allProducts')

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: "Try again!"
                    })
                }
            })
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
            .then(response => response.json())
            .then(result => {
                setName1(result.name)
                setDescription1(result.description)
                setPrice1(result.price)
                setQty1(result.qty)
            })
    }, [id])

    useEffect(() => {
        if (name !== '' && description !== '' && price !== '' && qty !== '') {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [name, description, price, qty])

    // function handlePhotoChange(event) {
    //     setImage(event.target.files[0]);
    // }

    return (
        <>
            <Meta title={"Add Product"} />
            <h1 className='dash-text-color pb-4 text-center'>Edit Product</h1>
            <div className="container-xxl">
                <div className="row">
                    <div className="col-12">
                        <Form className='filter-form d-flex flex-column gap-20' onSubmit={event => editProduct(event)}>
                            <div className="container-xxl">
                                <div className="row">
                                    <div className="col-6 pt-3 dash-text-color">
                                        <h3 className='pb-3'><b> Product Title:</b> <br /> {name1}</h3>
                                        <h3 className='pb-3'><b> Description: </b><br /> {description1}</h3>
                                        <h3 className='pb-3'><b> Price: </b><br /> {price1}</h3>
                                        <h3 className='pb-3'><b> Inventory:</b> <br /> {qty1}</h3>
                                    </div>
                                    <div className="col-6">
                                        <Form.Group controlId="name">
                                            <Form.Label className='dash-text-color dash-text-size'><b>Product Title:</b></Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={name}
                                                onChange={event => setName(event.target.value)}
                                                className='form-control'
                                                required
                                            />
                                        </Form.Group>
                                        <Form.Group className="mb-3" controlId="description">
                                            <Form.Label className='dash-text-color dash-text-size'><b>Description:</b></Form.Label>
                                            <Form.Control
                                                as="textarea"
                                                rows={5}
                                                value={description}
                                                onChange={event => setDescription(event.target.value)}
                                                className='form-control'
                                                required
                                            />
                                        </Form.Group>
                                        <Form.Group controlId="price">
                                            <Form.Label className='dash-text-color dash-text-size'><b>Price:</b></Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={price}
                                                onChange={event => setPrice(event.target.value)}
                                                className='form-control'
                                                required
                                            />
                                        </Form.Group>
                                        <Form.Group controlId="qty">
                                            <Form.Label className='dash-text-color dash-text-size'><b>Inventory:</b></Form.Label>
                                            <Form.Control
                                                type="text"
                                                value={qty}
                                                onChange={event => setQty(event.target.value)}
                                                className='form-control'
                                                required
                                            />
                                        </Form.Group>
                                    </div>
                                </div>
                            </div>
                            {/* <Form.Group controlId="image">
                                <Form.Label className='dash-text-color dash-text-size'>Photo</Form.Label>
                                <Form.Control
                                    type="file"
                                    accept="image/*"
                                    size="lg"
                                    onChange={handlePhotoChange}
                                    className='form-control'
                                />
                            </Form.Group> */}
                            <div className='d-flex justify-content-center gap-15 align-items-center'>
                            </div>
                            {isActive ?
                                <Button className='login-wide-button my-2' variant="success" type="submit" id="submitBtn">
                                    Submit
                                </Button>
                                :
                                <Button className='login-wide-button my-2' variant="success" type="submit" id="submitBtn" disabled>
                                    Submit
                                </Button>
                            }
                        </Form>
                    </div>
                </div>
            </div>

        </>
    )
}

export default EditProduct