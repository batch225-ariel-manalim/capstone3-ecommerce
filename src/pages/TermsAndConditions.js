import React from 'react'
import Meta from '../components/Meta';

const TermsAndConditions = () => {
    return (
        <>
            <Meta title={"Terms and Conditions"} />
            <section className='policy-wrapper py-5 home-wrapper-2'>
                <div className="container-xxl">
                    <div className="row">
                        <div className="col-12">
                            <div className="policy">
                                <h3 className='text-center'>Please Read the Terms And Condition:</h3>
                                <p>Welcome to our ecommerce website! Before using our site, please read the following terms and conditions carefully. By using our site, you are agreeing to these terms and conditions. <br />

                                    General Terms <br />
                                    1.1 These terms and conditions ("Agreement") govern your use of our website ("Site") and any products or services offered on our Site. <br />

                                    1.2 We reserve the right to modify, update or change the terms and conditions at any time without prior notice. <br />

                                    1.3 By using the Site, you acknowledge and agree that you have read, understood, and accept this Agreement. <br />

                                    User Accounts <br />
                                    2.1 In order to use certain features of the Site, you may need to create an account. <br />

                                    2.2 You are solely responsible for maintaining the confidentiality of your account information, including your password, and for any activity that occurs under your account. <br />

                                    2.3 You must provide accurate and complete information when creating your account. <br />

                                    2.4 You must not use another user's account without their permission. <br />

                                    2.5 We reserve the right to terminate any account that violates this Agreement or is deemed inappropriate in any way. <br />

                                    Orders and Payment <br />
                                    3.1 All orders placed through the Site are subject to acceptance by us. <br />

                                    3.2 We reserve the right to cancel or refuse any order at any time for any reason. <br />

                                    3.3 Payment must be made in full before we will process any order. <br />

                                    3.4 We may change our prices at any time without prior notice. <br />

                                    Shipping and Delivery <br />
                                    4.1 We will make every effort to ensure that your order is delivered in a timely manner. However, we cannot guarantee delivery dates. <br />

                                    4.2 We are not responsible for any customs or import duties that may be charged on international orders. <br />

                                    4.3 If an order is lost or damaged during shipping, we will work with you to resolve the issue. <br />

                                    Returns and Refunds <br />
                                    5.1 We have a return and refund policy in place for our products. <br />

                                    5.2 If you are not satisfied with your purchase, you may return it to us for a refund or exchange. <br />

                                    5.3 All returns must be in their original packaging and in new, unused condition. <br />

                                    5.4 Shipping and handling fees are non-refundable. <br />

                                    Intellectual Property <br />
                                    6.1 All content on the Site, including but not limited to text, graphics, logos, images, and software, is the property of the Site or its licensors. <br />

                                    6.2 You may not use any content on the Site without our express written consent. <br />

                                    Limitation of Liability <br />
                                    7.1 In no event shall the Site or its affiliates be liable for any direct, indirect, punitive, incidental, special, or consequential damages arising out of or in any way connected with the use of the Site. <br />

                                    7.2 You agree to indemnify and hold harmless the Site and its affiliates from any and all claims, damages, expenses, and costs, including reasonable attorneys' fees, arising from or related to your use of the Site. <br />

                                    Governing Law <br />
                                    8.1 This Agreement shall be governed by and construed in accordance with the laws of the jurisdiction in which the Site is located. <br />

                                    8.2 Any disputes arising out of or related to this Agreement shall be resolved in the courts of the jurisdiction in which the Site is located. <br />

                                    Termination <br />
                                    9.1 We may terminate this Agreement at any time without prior notice. <br />

                                    9.2 Upon termination, you must immediately cease all use of the Site. <br />

                                    Entire Agreement <br />
                                    10.1 This Agreement constitutes the entire agreement between you and the Site and supersedes all prior agreements or understandings, whether written or oral. <br />

                                    10.2 Any waiver of any provision of this Agreement will be effective only if in writing and signed by the Site. <br />

                                    Contact Us <br />
                                    11.1 If you have any questions or concerns regarding this Agreement or the Site, please contact us using the contact information provided on the Site. <br />

                                    11.2 We will make every effort to respond to your inquiry in a timely manner. <br />

                                    Thank you for using our ecommerce website. We hope you have a great experience shopping with us!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default TermsAndConditions