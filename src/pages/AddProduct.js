import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { useState, useEffect} from 'react';
import { useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import Meta from '../components/Meta';

const AddProduct = () => {
    const navigate = useNavigate();
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [image, setImage] = useState('');
    const [qty, setQty] = useState('');
    const [isActive, setIsActive] = useState(false);

    function addProduct(event) {
        event.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/products/addProduct`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                qty: qty,
                image: image
            })
        })
            .then(response => response.json())
            .then(result => {
                setName('');
                setDescription('');
                setPrice('');
                setQty('');
                setImage('');

                if (result) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Product Successfully Added',
                        showConfirmButton: false,
                        timer: 1500
                    })

                    navigate('/admin/allProducts')

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: "Try again!"
                    })
                }
            })
    }

    useEffect(() => {
        if (name !== '' && description !== '' && price !== '' && qty !== '') {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [name, description, price, qty])

    function handlePhotoChange(event) {
        setImage(event.target.files[0]);
    }

    return (
        <>
            <Meta title={"Add Product"} />
            <h1 className='dash-text-color pb-4'>Add Product</h1>
            <div className="container-xxl">
                <div className="row">
                    <div className="col-12">
                        <Form className='filter-form d-flex flex-column gap-20' onSubmit={event => addProduct(event)}>
                            <Form.Group controlId="name">
                                <Form.Label className='dash-text-color dash-text-size'>Product Title</Form.Label>
                                <Form.Control
                                    type="text"
                                    value={name}
                                    onChange={event => setName(event.target.value)}
                                    className='form-control'
                                    required
                                />
                            </Form.Group>
                            <Form.Group className="mb-3" controlId="description">
                                <Form.Label className='dash-text-color dash-text-size'>Description</Form.Label>
                                <Form.Control 
                                as="textarea" 
                                rows={5}
                                value={description}
                                onChange={event => setDescription(event.target.value)}
                                className='form-control'
                                required
                                />
                            </Form.Group>
                            <Form.Group controlId="price">
                                <Form.Label className='dash-text-color dash-text-size'>Price</Form.Label>
                                <Form.Control
                                    placeholder='&#8369;'
                                    type="text"
                                    value={price}
                                    onChange={event => setPrice(event.target.value)}
                                    className='form-control'
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="qty">
                                <Form.Label className='dash-text-color dash-text-size'>Quantity</Form.Label>
                                <Form.Control
                                    type="text"
                                    value={qty}
                                    onChange={event => setQty(event.target.value)}
                                    className='form-control'
                                    required
                                />
                            </Form.Group>
                            {/* <Form.Group controlId="image">
                                <Form.Label className='dash-text-color dash-text-size'>Photo</Form.Label>
                                <Form.Control
                                    type="file"
                                    accept="image/*"
                                    size="lg"
                                    onChange={handlePhotoChange}
                                    className='form-control'
                                />
                            </Form.Group> */}
                            <div className='d-flex justify-content-center gap-15 align-items-center'>
                            </div>
                            {isActive ?
                                <Button className='login-wide-button my-2' variant="success" type="submit" id="submitBtn">
                                    Create Product
                                </Button>
                                :
                                <Button className='login-wide-button my-2' variant="success" type="submit" id="submitBtn" disabled>
                                    Create Product
                                </Button>
                            }
                        </Form>
                    </div>
                </div>
            </div>

        </>
    )
}

export default AddProduct