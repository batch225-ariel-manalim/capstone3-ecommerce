import React from 'react';
import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import Meta from '../components/Meta';
import { Link, Redirect } from 'react-router-dom';
import Typed from "react-typed";

export default function Login() {
    const { user, setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    const retrieveUser = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: { Authorization: `Bearer ${token}` }
        })
            .then(res => res.json())
            .then(data => {
                setUser({ id: data.id, isAdmin: data.isAdmin });
            })
    }

    function authenticate(event) {
        event.preventDefault()
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(response => response.json())
            .then(result => {
                if (typeof result.access !== "undefined") {
                    localStorage.setItem('token', result.access)
                    retrieveUser(result.access)
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        iconColor: 'success',
                        title: 'Login Successfully',
                        text: 'Welcome Back!',
                        showConfirmButton: false,
                        timer: 2000
                    })
                } else {
                    Swal.fire({
                        title: 'Login Failed',
                        icon: 'error',
                        text: 'Invalid Email or Password'
                    })
                }
            })
    }


    useEffect(() => {
        if ((email !== '' && password !== '')) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    return (
        (user.id !== null) ?
            ((user.isAdmin) ?
                <Navigate to='/admin' />
                :
                <Navigate to="/" />
            )
            :
            <>
                <Meta title={"Login"} />
                <section className="login-main position-relative">
                    <img src="images/bg-main.jpg" className='img-fluid' alt="background-img" />
                    <div className='container-xxl main-login position-absolute'>
                        <div className='row'>
                            <div className='col-8'>
                                <div className='position-relative'>
                                    <div className='position-absolute'>
                                        <div className="type-effect mb-0">
                                            <Typed
                                                strings={[
                                                    'Affordable prices are waiting for you...',
                                                    "Free Shipping Natiowide",
                                                    'Customize your PC at your own pace now.'
                                                ]}
                                                typeSpeed={50}
                                                backSpeed={15}
                                                loop
                                            />
                                        </div>
                                        <h1 className='text-white'>Build Your <br />dream <br />Computer now</h1>
                                        <Link className='link-button' to='/'>Main Page</Link>
                                    </div>
                                </div>
                            </div>
                            <div className=" col-4 login-wrapper py-5 d-block justify-content-between flex-wrap align-items-center">
                                <div className="auth-card">
                                    <h2 className='mb-3'>Login</h2>
                                    <p className='login-text-color'>
                                        New user? <a className='login-links' href="/register">Create an Account</a>
                                    </p>
                                    <Form className='d-flex flex-column gap-15' onSubmit={event => authenticate(event)}>
                                        <Form.Group controlId="userEmail">
                                            <Form.Label className='login-text-color'>Email</Form.Label>
                                            <Form.Control
                                                type="email"
                                                value={email}
                                                onChange={event => setEmail(event.target.value)}
                                                className='form-control'
                                                required
                                            />
                                        </Form.Group>
                                        <Form.Group className='mt-1' controlId="password">
                                            <Form.Label className='login-text-color'>Password</Form.Label>
                                            <Form.Control
                                                type="password"
                                                value={password}
                                                onChange={event => setPassword(event.target.value)}
                                                className='form-control'
                                                required />
                                        </Form.Group>
                                        <p className='login-text-color pt-4'>Forgot password? <a className='login-links' href="forgot-password">Reset now</a></p>
                                        <div className='d-flex justify-content-center gap-15 align-items-center'>
                                        </div>
                                        {isActive ?
                                            <Button className='login-wide-button my-2' variant="danger" type="submit" id="submitBtn">
                                                Sign in
                                            </Button>
                                            :
                                            <Button className='login-wide-button my-2' variant="danger" type="submit" id="submitBtn" disabled>
                                                Sign in
                                            </Button>
                                        }
                                    </Form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section >
            </>
    )
}

